package main

import "testing"

func TestFeature(t *testing.T) {
	result := sayHello("toto")
	if result != "hello toto !\n" {
		t.Fail()
	}
}

func TestFeatureOther(t *testing.T) {
	result := sayHello("boby")
	if result != "hello boby !\n" {
		t.Fail()
	}
}
